import { IUser } from "../auth/IUser";
import { UserRole } from "../auth/UserRole";
import { IUserDatabase } from "../auth/IUserDatabase";

export default class FakeData implements IUserDatabase {
  readonly users: Map<string, IUser> = new Map<string, IUser>();

  public loadFakeData() {
    const user1: IUser = {
      username: "user",
      password: "user",
      role: UserRole.USER,
    };
    const user2: IUser = {
      username: "hr",
      password: "hr",
      role: UserRole.HR,
    };
    const user3: IUser = {
      username: "ceo",
      password: "ceo",
      role: UserRole.SUPER_COOL_CEO,
    };

    this.users.set(user1.username, user1);
    this.users.set(user2.username, user2);
    this.users.set(user3.username, user3);
  }

  private fakeDBAccessDelay(duration: number) {
    return new Promise<void>((resolve) => {
      setTimeout(resolve, duration);
    });
  }

  async getUserByUsername(username: string) {
    await this.fakeDBAccessDelay(1500);
    return this.users.get(username) ?? null;
  }

  async getAllUsers() {
    await this.fakeDBAccessDelay(1500);
    return Array.from(this.users.values());
  }
}
