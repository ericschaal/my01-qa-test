import { Box, Button, Center, Text, VStack } from "native-base";
import React from "react";
import AuthorizationService from "../../../auth/AuthorizationService";
import { Alert } from "react-native";
import { Action } from "../../../action/Action";
import AuthenticationService from "../../../auth/AuthenticationService";
import { useAppSelector } from "../../store/hooks";
import { selectCurrentUser } from "../login/loginSlice";

export interface IActionsScreenProps {
  authz: AuthorizationService;
  auth: AuthenticationService;
}

export const ActionsScreen = ({ auth, authz }: IActionsScreenProps) => {
  const user = useAppSelector(selectCurrentUser)!;

  const runAction = (action: Action) => {
    const isAllowed = authz.isUserAllowedToPerformAction(user, action);
    if (isAllowed) {
      Alert.alert("Success", "Done :)");
    } else {
      Alert.alert(
        "Unauthorized",
        `You are not authorized to perform this action.`
      );
    }
  };

  return (
    <Box ml={5} mr={5} safeArea>
      <VStack space="2">
        <Text fontSize="3xl" fontWeight="bold">
          Welcome back, {user.username}
        </Text>
        <Text fontSize="md" fontWeight="normal">
          Tell me what you wish to do today.
        </Text>
      </VStack>
      <Center>
        <VStack mt={5} space={2}>
          <Button onPress={() => runAction(Action.TALK_TO_INVESTORS)}>
            Talk To Investors.
          </Button>

          <Button onPress={() => runAction(Action.MAKE_COFFEE)}>
            Make Coffee
          </Button>

          <Button onPress={() => runAction(Action.HIRE_EMPLOYEE)}>
            Hire Employee
          </Button>

          <Button onPress={() => auth.logout()}>Logout</Button>
        </VStack>
      </Center>
    </Box>
  );
};
