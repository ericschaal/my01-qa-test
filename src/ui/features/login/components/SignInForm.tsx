import { Button, Center, Checkbox, Text, VStack } from "native-base";
import React, { useState } from "react";
import FloatingLabelInput from "../../../components/FloatingLabelTextInput";
import {
  PasswordIcon,
  ShowPasswordIcon,
  UsernameIcon,
} from "./SignInCardIcons";
import { SignInLoadingIndicator } from "./SignInLoadingIndicator";

export interface ISignInFormProps {
  onUsernameTextChanged: (txt: string) => void;
  usernameValue: string;
  onPasswordTextChanged: (txt: string) => void;
  passwordValue: string;
  onRememberMePressed: (value: boolean) => void;
  rememberMeValue: boolean;
  onSubmitPressed: () => void;
  onForgotPasswordPressed: () => void;
  loading: boolean;
}

export const SignInForm = ({
  usernameValue,
  onUsernameTextChanged,
  passwordValue,
  onPasswordTextChanged,
  onSubmitPressed,
  rememberMeValue,
  onRememberMePressed,
  loading,
}: ISignInFormProps) => {
  const [showPassword, setShowPassword] = useState<boolean>(false);

  return (
    <VStack space={{ base: "3" }}>
      <VStack space={{ base: "7" }}>
        <FloatingLabelInput
          isRequired
          isDisabled={loading}
          textContentType="username"
          autoCapitalize="none"
          label="Username"
          labelColor="#9ca3af"
          labelBackgroundColor="#fff"
          borderRadius="4"
          value={usernameValue}
          InputLeftElement={<UsernameIcon />}
          onChangeText={onUsernameTextChanged}
        />
        <FloatingLabelInput
          isRequired
          type={showPassword ? "" : "password"}
          label="Password"
          textContentType="password"
          value={passwordValue}
          isDisabled={loading}
          onChangeText={onPasswordTextChanged}
          InputLeftElement={<PasswordIcon />}
          InputRightElement={
            <ShowPasswordIcon
              showPass={showPassword}
              setShowPass={setShowPassword}
            />
          }
        />
      </VStack>
      <Checkbox
        isChecked={rememberMeValue}
        value="demo"
        mt="5"
        _checked={{
          value: "demo",
          backgroundColor: "primary.400",
          borderColor: "primary.400",
        }}
        onChange={onRememberMePressed}
        accessibilityLabel="Remember me"
      >
        <Text pl="3" fontWeight="normal" color="coolGray.800">
          Remember me and keep me logged in
        </Text>
      </Checkbox>
      {loading ? (
        <Center>
          <SignInLoadingIndicator mt={5} />
        </Center>
      ) : (
        <Button
          mt="5"
          size="md"
          borderRadius="4"
          backgroundColor="primary.400"
          _pressed={{
            backgroundColor: "primary.300",
          }}
          onPress={onSubmitPressed}
        >
          SIGN IN
        </Button>
      )}
    </VStack>
  );
};
