import React from "react";
import { Divider, HStack, Text } from "native-base";

export const SignInCardAlternateLoginOptions = () => (
  <HStack
    mt="5"
    space="2"
    mb={{ base: 6, md: 7 }}
    alignItems="center"
    justifyContent="center"
  >
    <Divider w="30%" color="coolGray.200" />
    <Text fontWeight="medium" color="coolGray.300">
      or
    </Text>
    <Divider w="30%" color="coolGray.200" />
  </HStack>
);
