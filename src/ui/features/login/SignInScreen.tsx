import React from "react";
import { Box, Stack, View } from "native-base";
import { StyleSheet } from "react-native";
import AuthenticationService from "../../../auth/AuthenticationService";
import { SignInHeader } from "./components/SignInHeader";
import { SignInCard } from "./components/SignInCard";

export interface ISignInScreenProps {
  auth: AuthenticationService;
}

export const SignInScreen = ({ auth }: ISignInScreenProps) => {
  return (
    <View style={[styles.container, { backgroundColor: "primary-400" }]}>
      <Box safeAreaTop backgroundColor="primary.400" flex="1">
        <Stack flexDirection="column" w="100%" flex="1">
          <SignInHeader />
          <SignInCard auth={auth} />
        </Stack>
      </Box>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1 },
  contentContainer: { flexGrow: 1 },
});
