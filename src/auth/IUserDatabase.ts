import { IUser } from "./IUser";

export interface IUserDatabase {
  getUserByUsername: (username: string) => Promise<IUser | null>;
  getAllUsers: () => Promise<IUser[]>;
}
