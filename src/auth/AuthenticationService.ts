import { IUserDatabase } from "./IUserDatabase";
import { store } from "../ui/store/store";
import { loginActions } from "../ui/features/login/loginSlice";

export default class AuthenticationService {
  constructor(private readonly userDb: IUserDatabase) {}

  private comparePasswords(p1: string, p2: string) {
    return p1 === p2;
  }

  async login(username: string, password: string) {
    const loadedUser = await this.userDb.getUserByUsername(username);
    if (loadedUser === null) {
      throw new Error(`User ${username} could not be found.`);
    } else {
      const isPasswordEqual = this.comparePasswords(
        password,
        loadedUser.password
      );
      if (!isPasswordEqual) {
        throw new Error("Invalid password.");
      }
      store.dispatch(loginActions.userLogin(loadedUser));
    }
  }

  async logout() {
    store.dispatch(loginActions.userLogout());
  }
}
