import { Action } from "../action/Action";
import { UserRole } from "./UserRole";
import { IUser } from "./IUser";
import { binarySearch } from "../utils/binarySearch";

export default class AuthorizationService {
  getAllowedRolesForAction(action: Action) {
    switch (action) {
      case Action.HIRE_EMPLOYEE:
        return [UserRole.HR];
      case Action.MAKE_COFFEE:
        return [UserRole.USER, UserRole.HR, UserRole.SUPER_COOL_CEO];
      case Action.TALK_TO_INVESTORS:
        return [UserRole.SUPER_COOL_CEO];
    }
  }

  isUserAllowedToPerformAction(user: IUser, action: Action) {
    const requiredRoles = this.getAllowedRolesForAction(action).sort();
    return requiredRoles[binarySearch(requiredRoles, user.role)] != undefined;
  }
}
