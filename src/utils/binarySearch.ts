export const binarySearch = (
  nums: Array<number>,
  key: number,
  indexOffset: number = 0
): number => {
  let index = Math.floor(nums.length / 2);
  if (nums[index] === key) {
    return index + indexOffset;
  }
  if (nums.length <= 1) {
    return -1;
  }
  if (nums[index] > key) {
    return binarySearch(nums.slice(0, index), key);
  } else {
    return binarySearch(nums.slice(index), key, indexOffset + index);
  }
};
