import React from "react";
import { NativeBaseProvider } from "native-base";
import { MY01Theme } from "./src/ui/theme";
import { Provider } from "react-redux";
import { store } from "./src/ui/store/store";
import { Root } from "./src/ui/Root";

const App = () => {
  return (
    <Provider store={store}>
      <NativeBaseProvider theme={MY01Theme}>
        <Root />
      </NativeBaseProvider>
    </Provider>
  );
};

export default App;
